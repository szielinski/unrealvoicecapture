// Copyright Frodst3D. All Rights Reserved 2021.
#include "VoiceCaptureComponent.h"

#include "Engine.h"
#include "Sound/SoundWaveProcedural.h"
#include "VoiceModule.h"

DEFINE_LOG_CATEGORY(LogVoiceCaptureComponent);

// Sets default values
UVoiceCaptureComponent::UVoiceCaptureComponent()
	: VoiceSampleRate(16000)
	, VoiceNumChannels(1)
	, bEnableVoiceAutoDetect(false)
	, bIsCapturing(false)
	, bIsSpeaking(false)
{
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bStartWithTickEnabled = false;

	DeviceName = TEXT("");//choose default
	microphoneThreshold = 1.0f;
	microphoneEnabled = true;
}

// Called when the game starts or when spawned
void UVoiceCaptureComponent::BeginPlay()
{
	Super::BeginPlay();
	ensure(!VoiceCapture.IsValid());	
	InitCapture();
}

void UVoiceCaptureComponent::InitCapture()
{
	if (VoiceCapture.IsValid()) {
		StopCapture();
		VoiceCapture.Reset();
		UE_LOG(LogTemp, Warning, TEXT("Voice Capture Reset: %s"), *DeviceName);
	}
	//FVoiceModule::~IModuleInterface();
	VoiceCapture = FVoiceModule::Get().CreateVoiceCapture(DeviceName, VoiceSampleRate, VoiceNumChannels);
	if (VoiceCapture) { UE_LOG(LogTemp, Warning, TEXT("Voice Capture Initialized: %s"), *DeviceName); } else { UE_LOG(LogTemp, Warning, TEXT("Warning: Voice Capture NOT Initialized")); }
	if (bEnableVoiceAutoDetect) StartCapture();
}

void UVoiceCaptureComponent::StartCapture()
{
	if (VoiceCapture.IsValid())
	{
		SoundStreaming = CreateSoundOutput(VoiceSampleRate, VoiceNumChannels);

		SetComponentTickEnabled(true);
		VoiceCapture->Start();

		bIsCapturing = true;
		EndSpeaking();
	} else UE_LOG(LogTemp, Warning, TEXT("Warning: Unable to start capture."));
}

void UVoiceCaptureComponent::StopCapture()
{
	if (VoiceCapture.IsValid() && VoiceCapture->IsCapturing())
	{		
		VoiceCapture->Stop();
		SetComponentTickEnabled(false);
		bIsCapturing = false;
		EndSpeaking();
		if (SoundStreaming->GetAvailableAudioByteCount()>0)
			OnNotifyVoiceData.Broadcast(SoundStreaming);
	}
}

void UVoiceCaptureComponent::MicThreshold(float treshold)
{
	microphoneThreshold = treshold;
}

void UVoiceCaptureComponent::MicrophoneEnabled(bool enabled)
{
	microphoneEnabled = enabled;
}

void UVoiceCaptureComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (VoiceCapture.IsValid() && VoiceCapture->IsCapturing())
	{
		uint32 NewVoiceDataBytes = 0;
		EVoiceCaptureState::Type MicState = VoiceCapture->GetCaptureState(NewVoiceDataBytes);
		if (MicState == EVoiceCaptureState::Ok && NewVoiceDataBytes > 0 && microphoneEnabled)
		{
			TArray<uint8> RawCaptureData;
			RawCaptureData.Empty(NewVoiceDataBytes);
			RawCaptureData.AddUninitialized(NewVoiceDataBytes);
			// Add new data at the beginning of the last frame			
			MicState = VoiceCapture->GetVoiceData(RawCaptureData.GetData(), NewVoiceDataBytes, NewVoiceDataBytes);
			bool threshold = AdjustVolume((int16 *)RawCaptureData.GetData(), NewVoiceDataBytes/2, microphoneThreshold);
			if (bIsSpeaking || threshold) {
				//================= Adjust the Volume ==================
				voiceCaptureTime += DeltaTime;
				SoundStreaming->QueueAudio(RawCaptureData.GetData(), NewVoiceDataBytes);
				if (!bIsSpeaking)
				{
					OnNotifyStartSpeak.Broadcast();
					bIsSpeaking = true; silienceTime = 0.0f;
				} else if ((!threshold)&&(bEnableVoiceAutoDetect)) {
					silienceTime += DeltaTime; 
					if ((silienceTime > 0.6f)||(voiceCaptureTime>5.0f)) { // jesli cisz d�u�sza ni� 0.6 albo dialog d�u�szy ni� 5s
						OnNotifyVoiceData.Broadcast(SoundStreaming);//send current sound data
						SoundStreaming = CreateSoundOutput(VoiceSampleRate, VoiceNumChannels);//create new sound data
						EndSpeaking(); UE_LOG(LogTemp, Warning, TEXT("End mic exit 1."));
					}
				} else silienceTime = 0.0f;
				//bIsSpeaking = true;
			}
		}
		else if (bEnableVoiceAutoDetect && bIsSpeaking ) {
			silienceTime += DeltaTime;
			if ((silienceTime > 0.6f) || (voiceCaptureTime > 5.0f)) {
				OnNotifyVoiceData.Broadcast(SoundStreaming);//send current sound data
				SoundStreaming = CreateSoundOutput(VoiceSampleRate, VoiceNumChannels);//create new sound data
				EndSpeaking(); UE_LOG(LogTemp, Warning, TEXT("End mic exit 2."));
			} else if (VoiceCapture->GetCurrentAmplitude() < 0.01f) {//check that we were speaking
				OnNotifyVoiceData.Broadcast(SoundStreaming);//send current sound data
				SoundStreaming = CreateSoundOutput(VoiceSampleRate, VoiceNumChannels);//create new sound data
				UE_LOG(LogTemp, Warning, TEXT("End mic exit 3 silience: %f"), silienceTime);
				EndSpeaking();
			}
		}
	}
}

bool UVoiceCaptureComponent::AdjustVolume(int16 *data, int32 length, float volume)
{
	int32 sum = 0;
	for (int32 l = 0; l < length; ++l) sum += abs(data[l]);
	if(length>0) sum = sum / length;
	int32 threshold = 8000 * volume;
	//UE_LOG(LogTemp, Warning, TEXT("Data: %d %d %d sum: %d threshold %d len: %d"), data[0], data[1], data[2], sum, threshold, length);
	if (sum < threshold) return(false); else return(true);
}

void UVoiceCaptureComponent::EndSpeaking()
{
	bIsSpeaking = false;
	silienceTime = 0.0f;
	voiceCaptureTime = 0.0f;
}


USoundWaveProcedural* UVoiceCaptureComponent::CreateSoundOutput(uint32 SampleRate, int32 NumChannels)
{
	USoundWaveProcedural* SoundOut = NewObject<USoundWaveProcedural>();
	SoundOut->SetSampleRate(SampleRate);
	SoundOut->NumChannels = NumChannels;
	SoundOut->Duration = 0.0f;
	SoundOut->SoundGroup = SOUNDGROUP_Voice;
	SoundOut->bLooping = false;	
	SoundOut->bCanProcessAsync = true;
	//this will be needed if we ever support streamed audio
	//SoundOut->OnSoundWaveProceduralUnderflow.BindUObject(this, &UVirBeingComponent::GenerateAudioData);
	//SoundOut->Duration = INDEFINITELY_LOOPING_DURATION;

	return SoundOut;
}