// Copyright Frost3D. All Rights Reserved 2021.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Runtime/Online/HTTP/Public/Http.h"
#include "Runtime/Core/Public/Misc/Guid.h"
#include "Sound/SoundWave.h"
#include "Misc/DateTime.h"
#include "VirbeApi.h"

#include "VoiceCaptureComponent.generated.h"

class IVoiceCapture;
class UAudioComponent;
class USoundWaveProcedural;

DECLARE_LOG_CATEGORY_EXTERN(LogVoiceCaptureComponent, Log, All);

/** OnNotifySpeech notification (phrase, word) */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FVoiceDataNotify, const USoundWave*, VoiceData);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FVoiceDetectedNotify);

/**
 Sample Actor Component for voice capturing.
*/
UCLASS(
	ClassGroup = (Custom),
	meta = (BlueprintSpawnableComponent)
)
class VIRBEING_API UVoiceCaptureComponent : public UActorComponent
{
	GENERATED_BODY()
	
public:	
	UVoiceCaptureComponent();

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
public:
	/** Device name: leave empty for default capturing device */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Voice Capture")
	FString DeviceName;

	/** Capture sampling rate */
	UPROPERTY(EditAnywhere, Category = "Voice Capture")
	uint32 VoiceSampleRate;

	/** Number of captured channels */
	UPROPERTY(EditAnywhere, Category = "Voice Capture")
	int32 VoiceNumChannels;

	/** When enabled, voice recording will start/stop automatically */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Voice Capture")
	bool bEnableVoiceAutoDetect;

	/** Returns true if mic is enabled */
	UPROPERTY(BlueprintReadOnly, Category = "Voice Capture")
	bool bIsCapturing;

	/** Returns true if voice data is coming */
	UPROPERTY(BlueprintReadOnly, Category = "Voice Capture")
	bool bIsSpeaking;


	UFUNCTION(BlueprintCallable, Category = "Voice Capture")
	void InitCapture();
	/**
	* Starts talking, will call OnEndSpeak when it finishes
	* @return true if bot can speak
	*/
	UFUNCTION(BlueprintCallable, Category = "Voice Capture")
	void StartCapture();


	/**
	* Starts talking, will call OnEndSpeak when it finishes
	* @return true if bot can speak
	*/
	UFUNCTION(BlueprintCallable, Category = "Voice Capture")
	void StopCapture();

	UFUNCTION(BlueprintCallable, Category = "Voice Capture")
	void MicThreshold(float treshold);

	UFUNCTION(BlueprintCallable, Category = "Voice Capture")
	void MicrophoneEnabled(bool enabled);

	/** Called when captured data is ready.  */
	UPROPERTY(BlueprintAssignable)
	FVoiceDataNotify OnNotifyVoiceData;

	/** Called when detected voice. */
	UPROPERTY(BlueprintAssignable)
	FVoiceDetectedNotify OnNotifyStartSpeak;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	/** Active audio capture */
	TSharedPtr<IVoiceCapture> VoiceCapture;

	/** Reference to the streaming sound */
	UPROPERTY()
	USoundWaveProcedural* SoundStreaming;

	float microphoneThreshold;
	bool microphoneEnabled;
	float silienceTime;
	float voiceCaptureTime;
	bool AdjustVolume(int16* data, int32 length, float volume);
	void EndSpeaking();
	USoundWaveProcedural* CreateSoundOutput(uint32 SampleRate, int32 NumChannels);
};
